<?php

/**
 * @file
 * Functions to support Marvelous theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function marvelous_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $form['#attached']['library'][] = 'marvelous/theme.setting';

  // >>>>>>>>>>>>>>>>Header color>>>>>>>>
  $form['show_header_color']['header_colors'] = [
    '#type' => 'details',
    '#title' => t('Header Background Color'),
    '#open' => FALSE,
  ];
  $form['show_header_color']['header_colors']['show_header_color'] = [
    '#type' => 'checkbox',
    '#title' => t('Want to Header Background Color'),
    '#default_value' => theme_get_setting('show_header_color', 'marvelous'),
    '#description'   => t("Show/Hide header color details"),
  ];
  $form['show_header_color']['header_colors']['header_color'] = [
    '#type' => 'color',
    '#title' => t('Background color'),
    '#default_value' => theme_get_setting('header_color', 'marvelous'),
    '#description' => t("Select Header Background Color"),
  ];

  // >>>>>>> Number of slides you want to display>>>>>>>>>
  $form['show_slide_num']['slide_num_display'] = [
    '#type' => 'details',
    '#required' => TRUE,
    '#title' => t('Number of Slide'),
    '#open' => FALSE,
  ];
  $form['show_slide_num']['slide_num_display']['show_slide_num'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Slider'),
    '#default_value' => theme_get_setting('show_slide_num', 'marvelous'),
    '#description' => t('Show/Hide'),
  ];
  $form['show_slide_num']['slide_num_display']['slide_num'] = [
    '#type' => 'textfield',
    '#title' => t('Enter Number of Slider you want to Display'),
    '#default_value' => theme_get_setting('slide_num', 'marvelous'),
    '#description' => t("Enter Number of Slider you want to Display"),
  ];
  $no = theme_get_setting('slide_num', 'marvelous');
  $display = theme_get_setting('show_slide_num', 'marvelous');

  // Slide show>>>>>>>>>>>>>>>>>.
  if ($no > 0 || $display) {
    $form['banner']['slideshow'] = [
      '#type' => 'details',
      '#title' => t('Slider'),
      '#open' => FALSE,
    ];
    $form['banner']['slideshow']['banner'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Slider'),
      '#default_value' => theme_get_setting('banner', 'marvelous'),
      '#description' => t('Show/hide Slider'),
    ];
    $form['banner']['slideshow']['hero_color'] = [
      '#type' => 'color',
      '#title' => t('Background-color'),
      '#default_value' => theme_get_setting('hero_color', 'marvelous'),
      '#description' => t("Select Hero color"),
    ];
    $form['banner']['slideshow']['slide'] = [
      '#markup' => t('You can change the description of each slide in the following Slide Setting fieldsets.'),

    ];
    for ($i = 1; $i <= $no; $i++) {
      $form['banner']['slideshow']['slide' . $i] = [
        '#type' => 'fieldset',
        '#title' => t('Slide  @i', ['@i' => $i]),
        '#attributes' => [
          'id' => 'slide' . $i,
          'class' => ['slide_show'],
        ],
      ];
      $form['banner']['slideshow']['slide' . $i]['slide' . $i . '_title'] = [
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#default_value' => theme_get_setting('slide' . $i . '_title', 'marvelous'),

      ];
      $form['banner']['slideshow']['slide' . $i]['slide' . $i . '_head'] = [
        '#type' => 'textfield',
        '#title' => t('Heading'),
        '#default_value' => theme_get_setting('slide' . $i . '_head', 'marvelous'),
      ];
      $form['banner']['slideshow']['slide' . $i]['slide' . $i . '_des'] = [
        '#type' => 'textfield',
        '#title' => t('Description'),
        '#default_value' => theme_get_setting('slide' . $i . '_des', 'marvelous'),
      ];
      $form['banner']['slideshow']['slide' . $i]['slide' . $i . '_url'] = [
        '#type' => 'textfield',
        '#title' => t('URL 1'),
        '#default_value' => theme_get_setting('slide' . $i . '_url', 'marvelous'),

      ];
      $form['banner']['slideshow']['slide' . $i]['slide' . $i . '_url_text'] = [
        '#type' => 'textfield',
        '#title' => t('URL Title'),
        '#default_value' => theme_get_setting('slide' . $i . '_url_text', 'marvelous'),
      ];
      $form['banner']['slideshow']['slide' . $i]['slide' . $i . '_url_1'] = [
        '#type' => 'textfield',
        '#title' => t('URL 2'),
        '#default_value' => theme_get_setting('slide' . $i . '_url_1', 'marvelous'),

      ];
      $form['banner']['slideshow']['slide' . $i]['slide' . $i . '_url_text_1'] = [
        '#type' => 'textfield',
        '#title' => t('URL Title'),
        '#default_value' => theme_get_setting('slide' . $i . '_url_text_1', 'marvelous'),
      ];

    }

  }
  else {
    $form['banner']['slideshow'] = [
      '#type' => 'details',
      '#title' => t('Slider'),
      '#open' => FALSE,
    ];

  }

  // >>>>>>>>>Header image >>>>>>>>>>>>>>>>>>>
  $form['show_header_image']['header_image'] = [
    '#type' => 'details',
    '#title' => t('Hero Section Image'),
    '#open' => FALSE,
  ];
  $form['show_header_image']['header_image']['show_header_image'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Hero Section Image'),
    '#default_value' => theme_get_setting('show_header_image', 'marvelous'),
    '#description' => t('Show/Hide'),
  ];
  $form['show_header_image']['header_image']['image'] = [
    '#type' => 'managed_file',
    '#title' => t('Hero Section Image'),
    '#default_value' => theme_get_setting('image', 'marvelous'),
    '#upload_location' => 'public://',
    '#upload_validators' => [
      'file_validate_extensions' => ['gif png jpg jpeg svg'],
    ],

  ];
  // >>>>>>>>>>>>>>>>Content color>>>>>>>>
  $form['show_content_color']['content_colors'] = [
    '#type' => 'details',
    '#title' => t('Content Background'),
    '#open' => FALSE,
  ];
  $form['show_content_color']['content_colors']['show_content_color'] = [
    '#type' => 'checkbox',
    '#title' => t('Want to Content Background Color'),
    '#default_value' => theme_get_setting('show_content_color', 'marvelous'),
    '#description'   => t("Show/Hide Header Color Details"),
  ];
  $form['show_content_color']['content_colors']['Content_color'] = [
    '#type' => 'color',
    '#title' => t('Background Color'),
    '#default_value' => theme_get_setting('Content_color', 'marvelous'),
    '#description' => t("Select Content Background Color manually"),
  ];

  // >>>>>>>>>>>>>>>>Footer color>>>>>>>>
  $form['show_Footer_color']['Footer_colors'] = [
    '#type' => 'details',
    '#title' => t('Footer Color'),
    '#open' => FALSE,
  ];
  $form['show_Footer_color']['Footer_colors']['show_footer_color'] = [
    '#type' => 'checkbox',
    '#title' => t('Want to Footer color'),
    '#default_value' => theme_get_setting('show_footer_color', 'marvelous'),
    '#description'   => t("Show/Hide Footer Details"),
  ];
  $form['show_Footer_color']['Footer_colors']['footer_color'] = [
    '#type' => 'color',
    '#title' => t('Footer Color'),
    '#default_value' => theme_get_setting('footer_color', 'marvelous'),
    '#description' => t("Select Footer Color"),
  ];

  // >>>>>>> Footer First >>>>>>>>>
  $form['show_footer_first_details']['footer_details'] = [
    '#type' => 'details',
    '#title' => t('Footer First Details'),
    '#open' => FALSE,
  ];
  $form['show_footer_first_details']['footer_details']['show_footer_first_details'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Footer Details'),
    '#default_value' => theme_get_setting('show_footer_first_details', 'marvelous'),
    '#description'   => t("Show/Hide Footer Details"),
  ];
  $form['show_footer_first_details']['footer_details']['footer_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Footer image'),
    '#default_value' => theme_get_setting('footer_image', 'marvelous'),
    '#upload_location' => 'public://',
    '#upload_validators' => [
      'file_validate_extensions' => ['gif png jpg jpeg'],
    ],
  ];

  $form['show_footer_first_details']['footer_details']['footer_text'] = [
    '#type' => 'textfield',
    '#title' => t('Company Name'),
    '#default_value' => theme_get_setting('footer_text', 'marvelous'),
    '#description' => t("Enter your Company Name"),
  ];
  $form['show_footer_first_details']['footer_details']['footer_email'] = [
    '#type' => 'email',
    '#title' => t('Company email'),
    '#default_value' => theme_get_setting('footer_email', 'marvelous'),
    '#description' => t("Enter your Company email"),
  ];
  $form['show_footer_first_details']['footer_details']['footer_phone'] = [
    '#type' => 'tel',
    '#title' => t('Company Phone'),
    '#default_value' => theme_get_setting('footer_phone', 'marvelous'),
    '#description' => t("Enter your Company Phone"),
  ];
  $form['show_footer_first_details']['footer_details']['footer_address'] = [
    '#type' => 'textfield',
    '#title' => t('Company Address'),
    '#default_value' => theme_get_setting('footer_address', 'marvelous'),
    '#description' => t("Enter your Company address"),
  ];

  // >>>>>>>>>>>Footer fourth >>>>>>>>
  $form['show_footer_fourth_details']['footer_fourth_details'] = [
    '#type' => 'details',
    '#title' => t('Footer Fourth Details'),
    '#open' => FALSE,
  ];
  $form['show_footer_fourth_details']['footer_fourth_details']['show_footer_fourth_details'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Footer Details'),
    '#default_value' => theme_get_setting('show_footer_fourth_details', 'marvelous'),
    '#description'   => t("Show/Hide Footer Details"),
  ];
  $form['show_footer_fourth_details']['footer_fourth_details']['footer_head'] = [
    '#type' => 'textfield',
    '#title' => t('Heading'),
    '#default_value' => theme_get_setting('footer_head', 'marvelous'),
    '#description' => t("Enter Footer Heading"),
  ];
  $form['show_footer_fourth_details']['footer_fourth_details']['footer_des'] = [
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => theme_get_setting('footer_des', 'marvelous'),
    '#description' => t("Enter Description"),
  ];

  // >>>>>>>>>>> Social Media Link
  $form['show_social_icon']['social_icon'] = [
    '#type' => 'details',
    '#title' => t('Social Media Link'),
    '#open' => FALSE,
  ];
  $form['show_social_icon']['social_icon']['show_social_icon'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Social Icons'),
    '#default_value' => theme_get_setting('show_social_icon', 'marvelous'),
    '#description'   => t("Show/Hide social media links"),
  ];
  $form['show_social_icon']['social_icon']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook Link'),
    '#default_value' => theme_get_setting('facebook_url', 'marvelous'),
  ];
  $form['show_social_icon']['social_icon']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter Link'),
    '#default_value' => theme_get_setting('twitter_url', 'marvelous'),
  ];
  $form['show_social_icon']['social_icon']['instagram_url'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram Link'),
    '#default_value' => theme_get_setting('instagram_url', 'marvelous'),
  ];
  $form['show_social_icon']['social_icon']['linkedin_url'] = [
    '#type' => 'textfield',
    '#title' => t('Linkedin Link'),
    '#default_value' => theme_get_setting('linkedin_url', 'marvelous'),
  ];

  // >>>>>>>>>>Footer copyright>>>>>>>>>
  $form['footer_details'] = [
    '#type' => 'details',
    '#title' => t('Copyright'),
    '#open' => FALSE,
  ];
  $form['footer_details']['footer_details'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Copyright'),
    '#default_value' => theme_get_setting('footer_details', 'marvelous'),
    '#description'   => t("Show/Hide Copyright"),
  ];
  $form['footer_details']['copyright_color'] = [
    '#type' => 'color',
    '#title' => t('Footer Bottom color'),
    '#default_value' => theme_get_setting('copyright_color', 'marvelous'),
    '#description' => t("SelectFooter Bottom Color"),
  ];

  $form['footer_details']['footer_copyright'] = [
    '#type' => 'textarea',
    '#title' => t('Footer Copyright:'),
    '#default_value' => theme_get_setting('footer_copyright', 'marvelous'),
    '#description' => t("Text area for Footer Copyright."),
  ];
  $form['footer_details']['footer_deve_text'] = [
    '#type' => 'textarea',
    '#title' => t('Developer Info'),
    '#default_value' => theme_get_setting('footer_deve_text', 'marvelous'),
    '#description' => t("Developer Info."),
  ];

  // >>>>>>>>>>>CSS STYLE >>>>>>>>
  $form['show_css_details']['footer_css_details'] = [
    '#type' => 'details',
    '#title' => t('CSS Style'),
    '#open' => FALSE,
  ];
  $form['show_css_details']['footer_css_details']['show_css_details'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Footer Details'),
    '#default_value' => theme_get_setting('show_css_details', 'marvelous'),
    '#description'   => t("Show/Hide Footer Details"),
  ];
  $form['show_css_details']['footer_css_details']['text_color'] = [
    '#type' => 'color',
    '#title' => t('Text Color'),
    '#default_value' => theme_get_setting('text_color', 'marvelous'),
    '#description' => t("You can change h1 to h6 , header menu color , p color"),
  ];
  $form['show_css_details']['footer_css_details']['font_Size'] = [
    '#type' => 'textfield',
    '#title' => t('Font_Size'),
    '#default_value' => theme_get_setting('font_Size', 'marvelous'),
    '#description' => t("You can not change Slider font size, Only change content P font size"),
  ];
  $form['show_css_details']['footer_css_details']['link_color'] = [
    '#type' => 'color',
    '#title' => t('Link Color'),
    '#default_value' => theme_get_setting('link_color', 'marvelous'),
    '#description' => t("You can change footer Link  color"),
  ];
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  $theme_file = \Drupal::service('extension.path.resolver')->getPath('theme', $theme) . '/marvelous.theme';
  $build_info = $form_state->getBuildInfo();
  if (!in_array($theme_file, $build_info['files'])) {
    $build_info['files'][] = $theme_file;
  }
  $form_state->setBuildInfo($build_info);
}
